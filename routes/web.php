<?php

use Illuminate\Http\File;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Users\ProfileController;
use App\Http\Controllers\Users\RoleController;
use App\Http\Controllers\Users\MessageController;
use App\Http\Controllers\Users\NotificationController;
use App\Http\Controllers\Site\HomeController;
use App\Http\Controllers\Site\DashboardController;
use App\Http\Controllers\Site\SubjectController;
use App\Http\Controllers\Site\WidgetController;
use App\Http\Controllers\Site\PageController;
use App\Http\Controllers\Site\OrderFieldController;
use App\Http\Controllers\Projects\OrderController;
use App\Http\Controllers\Projects\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', [WelcomeController::class, 'index'])->name('welcome');
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('home', [HomeController::class, 'index'])->name('home');
Route::get('order', [HomeController::class, 'order'])->name('order');
Route::put('ordernow', [HomeController::class, 'orderNow'])->name('ordernow');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {
        Route::get('/', [ProfileController::class, 'edit'])->name('edit');
        Route::put('/', [ProfileController::class, 'update'])->name('update');
        Route::put('password', [ProfileController::class, 'password'])->name('password');    
    });
    
    Route::resource('users', UserController::class, ['except' => ['show']]);
    Route::resource('roles', RoleController::class, ['except' => ['show', 'destroy']]);
    Route::resource('messages', MessageController::class, ['except' => ['show']]);
    Route::resource('notifications', NotificationController::class, ['except' => ['show']]);

    Route::resource('subjects', SubjectController::class, ['except' => ['show']]);
    Route::resource('widgets', WidgetController::class, ['except' => ['show']]);
    Route::resource('pages', PageController::class, ['except' => ['show']]);
    Route::resource('orderfields', OrderFieldController::class, ['except' => ['show']]);

    Route::resource('orders', OrderController::class, ['except' => ['show']]);
    Route::resource('tasks', TaskController::class, ['except' => ['show']]);
  
});

Route::get('storage/{filename}', function ($filename) {
    $path = storage_path('public/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});
