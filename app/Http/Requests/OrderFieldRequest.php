<?php

namespace App\Http\Requests;

use App\Models\Site\OrderField;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class OrderFieldRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /*'label' => [
                'required', 'min:3', Rule::unique((new OrderField)->getTable())->ignore($this->route()->OrderField->id ?? null)
            ],
            /*'description' => [
                'nullable', 'min:5'
            ]*/
        ];
    }
}
