<?php

namespace App\Http\Controllers\Site;

use App\Models\User;
use App\Models\Site\Subject;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubjectRequest;

class SubjectController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Subject::class);
    }

    /**
     * Display a listing of the subjects
     *
     * @param \App\Models\Subject  $model
     * @return \Illuminate\View\View
     */
    public function index(Subject $subject)
    {
        $this->authorize('manage-subjects', User::class);

        return view('site.subjects.index', ['subjects' => $subject->all()]);
    }

    /**
     * Show the form for creating a new subject
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('site.subjects.create');
    }

    /**
     * Store a newly created subject in storage
     *
     * @param  \App\Http\Requests\SubjectRequest  $request
     * @param  \App\Models\Subject  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SubjectRequest $request, Subject $subject)
    {
        $subject->create($request->all());

        return redirect()->route('site.subjects.index')->withStatus(__('Subject successfully created.'));
    }

    /**
     * Show the form for editing the specified subject
     *
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\View\View
     */
    public function edit(Subject $subject)
    {
        return view('site.subjects.edit', compact('subject'));
    }

    /**
     * Update the specified subject in storage
     *
     * @param  \App\Http\Requests\SubjectRequest  $request
     * @param  \App\Models\Subject  $subject
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SubjectRequest $request, Subject $subject)
    {
        $subject->update($request->all());

        return redirect()->route('site.subjects.index')->withStatus(__('Subject successfully updated.'));
    }
}
