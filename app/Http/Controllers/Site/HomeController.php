<?php

namespace App\Http\Controllers\Site;

use App\Models\Projects\Order;
use App\Models\Site\OrderField;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
  
    /**
     * Show the application home.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('site.home');
    }

    /**
     * Show the form for creating a new order
     *
     * @return \Illuminate\View\View
     */
    public function order()
    {
        $html_fields = '';
        $fields = OrderField::where('enabled', 1)->get();

        foreach($fields as $field)
        {
            $html_fields .= "\n";
            $html_fields .= '<div class="row">';
            $html_fields .= '<div class="col-sm-12">';
            $html_fields .= '<div class="form-group bmd-form-group field-' . $field->id .'-error " >';
            $html_fields .= "\n";

            $html_options = array();

            if (isset($field->options))
            {
                $html_options = explode('||', $field->options);
            }

            switch ($field->type)
            {
                case 'number':
                    $html_fields .= '<label>' . $field->label . '</label>';
                    $html_fields .= "\n";
                    $html_fields .= '<input type="number" class="form-control" id="field-' . $field->id .'" name="field-' . $field->id .'">';
                    break;

                case 'textarea':
                    $html_fields .= '<label>' . $field->label . '</label>';
                    $html_fields .= "\n";
                    $html_fields .= '<textarea class="form-control" id="field-' . $field->id .'" name="field-' . $field->id .'"></textarea>';
                    break;
    
                case 'checkbox':
                    $html_fields .= '<label class="form-check-label">';
                    $html_fields .= "\n";
                    $html_fields .= '<input class="form-check-input" type="checkbox" name="policy" value="1">
                    <span class="form-check-sign">
                      <span class="check"></span>
                    </span>I agree with the <a href="#">terms and conditions</a>
                  </label>';
                    break;

                case 'select':
                    $html_fields .= '<label>' . $field->label . '</label>';
                    $html_fields .= "\n";
                    $html_fields .= '<select class="selectpicker col-sm-12 pl-0 pr-0" data-style="select-with-transition" title="" data-size="100" id="field-' . $field->id .'" name="field-' . $field->id .'">';
                    $html_fields .= "\n";

                    foreach($html_options as $html_option)
                    {
                        $option = explode('#', trim($html_option));
                        $html_fields .= '<option value="' . $option[0] . '">' . $option[1] . '</option>';
                        $html_fields .= "\n";
                    }
                    $html_fields .= '</select>';
                    break;

                case 'select-radio':
                    $html_fields .= '<label>' . $field->label . '</label>';
                    $html_fields .= "\n";
                    foreach($html_options as $html_option)
                    {
                        $option = explode('#', trim($html_option));
                        $html_fields .= '<input type="radio" class="form-control" id="field-' . $field->id .'" name="field-' . $field->id .'" value="' . $option[0]. '">' . $option[1];
                        $html_fields .= "\n";
                    }
                    break;
                
                default:
                    $html_fields .= '<label>' . $field->label . '</label>';
                    $html_fields .= "\n";
                    $html_fields .= '<input type="text" class="form-control" id="field-' . $field->id .'" name="field-' . $field->id .'">';
                    break;
            }

            $html_fields .= "\n";
            $html_fields .= '</div>';
            $html_fields .= '</div>';
            $html_fields .= '</div>';
            $html_fields .= "\n";
        }

        return view('site.order', compact('html_fields'));
    }

    /**
     * Store a newly created order in storage
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Order $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function orderNow(Request $request)
    {
        try {
            $fields = OrderField::where('enabled', 1)->get();
            $fullorder = '';
            foreach($fields as $field)
            {
                $fullorder .= $field->label . ': ';
                $fullorder .= $request[$field->id];
                $fullorder .= '||';
            }

            Order::create([
                'name' => 'PRH Order',
                'content' => $fullorder,
            ]);
            return redirect()->route('home')->withStatus(__('Your Order was successfully submitted.'));
        } catch (\Exception $exception) {
            return redirect()->back()->withError('Error while creating Order');
        }
    }

}
