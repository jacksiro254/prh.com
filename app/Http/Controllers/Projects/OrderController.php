<?php

namespace App\Http\Controllers\Projects;

use App\Models\User;
use App\Models\Projects\Order;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Order::class);
    }

    /**
     * Display a listing of the orders
     *
     * @param \App\Models\Order  $model
     * @return \Illuminate\View\View
     */
    public function index(Order $order)
    {
        $this->authorize('manage-orders', User::class);

        return view('projects.orders.index', ['orders' => $order->all()]);
    }

    /**
     * Show the form for creating a new order
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('projects.orders.create');
    }

    /**
     * Store a newly created order in storage
     *
     * @param  \App\Http\Requests\OrderRequest  $request
     * @param  \App\Models\Order  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(OrderRequest $request, Order $order)
    {
        $order->create($request->all());

        return redirect()->route('projects.orders.index')->withStatus(__('Order successfully created.'));
    }

    /**
     * Show the form for editing the specified order
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\View\View
     */
    public function edit(Order $order)
    {
        return view('projects.orders.edit', compact('order'));
    }

    /**
     * Update the specified order in storage
     *
     * @param  \App\Http\Requests\OrderRequest  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(OrderRequest $request, Order $order)
    {
        $order->update($request->all());

        return redirect()->route('projects.orders.index')->withStatus(__('Order successfully updated.'));
    }
}
