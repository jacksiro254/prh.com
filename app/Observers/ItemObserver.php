<?php

namespace App\Observers;

use App\Models\Item;
use Illuminate\Support\Facades\File;

class ItemObserver
{
    /**
     * Handle the User "deleting" event.
     *
     * @param  \App\Item  $item
     * @return void
     */
    public function deleting(Item  $item)
    {
        File::delete(storage_path("/app/public/{$item->avatar}"));
        
        $item->tags()->detach();
    }

    /**
     * Handle the User "updating" event.
     *
     * @param  \App\Item  $item
     * @return void
     */
    public function updating(Item $item)
    {
        if ($item->avatar != $item->getOriginal('avatar')) {
            File::delete(storage_path("/app/public/{$item->getOriginal('avatar')}"));
        }
    }
}
