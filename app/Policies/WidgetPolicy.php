<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Site\Widget;
use Illuminate\Auth\Access\HandlesAuthorization;

class WidgetPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can see the widgets.
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can create widgets.
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the widget.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Widget  $widget
     * @return boolean
     */
    public function update(User $user, Widget $widget)
    {
        return $user->isAdmin();
    }
}
