<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Site\OrderField;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderFieldPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can see the orderfields.
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can create orderfields.
     *
     * @param  \App\Models\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the orderfield.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OrderField  $orderfield
     * @return boolean
     */
    public function update(User $user, OrderField $orderfield)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the orderfield.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OrderField  $orderfield
     * @return boolean
     */
    public function destroy(User $user, OrderField $orderfield)
    {
        return $user->isAdmin();
    }
}
