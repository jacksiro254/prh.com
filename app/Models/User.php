<?php

namespace App\Models;

use App\Models\Users\Role;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone_number', 'company', 'avatar' ,'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the role of the user
     *
     * @return \App\Models\Role
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'roleid', 'id');
    }

    /**
     * Get the path to the profile avatar
     *
     * @return string
     */
    public function profilePicture()
    {
        if ($this->avatar) {
            return asset("storage/{$this->avatar}");
        }

        return asset('storage/user.jpg');
    }

    /**
     * Check if the user has admin role
     *
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->roleid == 3;
    }

    /**
     * Check if the user has creator role
     *
     * @return boolean
     */
    public function isWriter()
    {
        return $this->roleid == 2;
    }

    /**
     * Check if the user has user role
     *
     * @return boolean
     */
    public function isClient()
    {
        return $this->roleid == 1;
    }

    public function truckmake() {
        return $this->hasMany(Truckmake::class); 
    }
    
    public function make() {
        return $this->hasMany(Make::class); 
    }

    public function modell() {
        return $this->hasMany(Modell::class); 
    }

    public function category() {
        return $this->hasMany(Category::class); 
    }

    public function specific() {
        return $this->hasMany(Specific::class); 
    }

    public function type() {
        return $this->hasMany(Type::class); 
    }

    public function deal() {
        return $this->hasMany(Deal::class); 
    }
}
