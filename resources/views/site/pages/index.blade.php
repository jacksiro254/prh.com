@extends('layouts.app', ['activePage' => 'page-management', 'menuParent' => 'laravel', 'titlePage' => __('Page Management')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">recent_actors</i>
                </div>
                <h4 class="card-title">{{ __('Pages') }}</h4>
              </div>
              <div class="card-body">
                @can('create', App\Models\Site\Page::class)
                  <div class="row">
                    <div class="col-12 text-right">
                      <a href="{{ route('pages.create') }}" class="btn btn-sm btn-rose">{{ __('Add A Page') }}</a>
                    </div>
                  </div>
                @endcan
                <div class="table-responsive">
                  <table id="datatables" class="table table-striped table-no-bordered table-hover" style="display:none">
                    <thead class="text-primary">
                      <th>
                          {{ __('Name') }}
                      </th>
                      <th>
                        {{ __('Url') }}
                      </th>
                      <th>
                        {{ __('Published') }}
                      </th>
                      <th>
                        {{ __('Creation date') }}
                      </th>
                      @can('manage-pages', App\Models\User::class)
                        <th class="text-right">
                          {{ __('Actions') }}
                        </th>
                      @endcan
                    </thead>
                    <tbody>
                      @foreach($pages as $page)
                        <tr>
                          <td>
                            {{ $page->name }}
                          </td>
                          <td>
                            {{ $page->name }}
                          </td>
                          <td>
                            {{ $page->published }}
                          </td>
                          <td>
                            {{ $page->created_at->format('Y-m-d') }}
                          </td>
                          @can('manage-pages', App\Models\User::class)
                            <td class="td-actions text-right">
                              @can('update', $page)
                                <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('pages.edit', $page) }}" data-original-title="" title="">
                                  <i class="material-icons">edit</i>
                                  <div class="ripple-container"></div>
                                </a>
                              @endcan
                            </td>
                          @endcan
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
<script>
  $(document).ready(function() {
    $('#datatables').fadeIn(1100);
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search Pages",
      },
      "columnDefs": [
        { "orderable": false, "targets": 3 },
      ],
    });
  });
</script>
@endpush