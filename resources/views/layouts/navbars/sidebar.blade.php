<div class="sidebar" data-color="azure" data-background-color="black" data-image="{{ asset('material') }}/img/bg.jpg">
    
    <div class="logo">
        <a href="{{ route('home') }}" class="text-center">
            <img src="{{ asset('material') }}/img/logo.png" style="width:200px;margin-left:25px;" alt="">
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{ auth()->user()->profilePicture() }}" />
            </div>
            <div class="user-info">
                <a data-toggle="collapse" href="#collapseExample" class="username">
                    <span>
                        {{ auth()->user()->name }}
                        <b class="caret"></b>
                    </span>
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('profile.edit') }}">
                                <span class="sidebar-mini"> MP </span>
                                <span class="sidebar-normal"> My Profile </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('profile.edit') }}">
                                <span class="sidebar-mini"> EP </span>
                                <span class="sidebar-normal"> Edit Profile </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <span class="sidebar-mini"> S </span>
                                <span class="sidebar-normal"> Settings </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">
            <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('dashboard') }}">
                    <i class="material-icons">dashboard</i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            
            @can ('manage-projects', App\Models\User::class)
            <li class="nav-item {{ ($menuParent ?? '' == 'laravel' || $activePage == 'dashboard') ? ' active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#projectManagement"
                    {{ ($menuParent ?? '' == 'laravel' || $activePage == 'dashboard') ? ' aria-expanded="true"' : '' }}>
                    <i class="material-icons">receipt</i>
                    <p>{{ __('Project Management') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ ($menuParent ?? '' == 'dashboard' || $menuParent ?? '' == 'laravel') ? ' show' : '' }}"
                    id="projectManagement">
                    <ul class="nav">
                        @can('manage-orders', App\Models\User::class)
                        <li class="nav-item{{ $activePage == 'order-management' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('orders.index') }}">
                                <span class="sidebar-normal"> {{ __('Order Management') }} </span>
                            </a>
                        </li>
                        @endcan
                        @can('manage-tasks', App\Models\User::class)
                        <li class="nav-item{{ $activePage == 'task-management' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('tasks.index') }}">
                                <span class="sidebar-normal"> {{ __('Tasks Management') }} </span>
                            </a>
                        </li>
                        @endcan

                    </ul>
                </div>
            </li>
            @endcan

            @can ('manage-site', App\Models\User::class)
            <li class="nav-item {{ ($menuParent ?? '' == 'laravel' || $activePage == 'dashboard') ? ' active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#siteManagement"
                    {{ ($menuParent ?? '' == 'laravel' || $activePage == 'dashboard') ? ' aria-expanded="true"' : '' }}>
                    <i class="material-icons">receipt</i>
                    <p>{{ __('Site Management') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ ($menuParent ?? '' == 'dashboard' || $menuParent ?? '' == 'laravel') ? ' show' : '' }}"
                    id="siteManagement">
                    <ul class="nav">
                        @can('manage-subjects', App\Models\User::class)
                        <li class="nav-item{{ $activePage == 'subject-management' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('subjects.index') }}">
                                <span class="sidebar-normal"> {{ __('Subject Management') }} </span>
                            </a>
                        </li>
                        @endcan
                        @can('manage-widgets', App\Models\User::class)
                        <li class="nav-item{{ $activePage == 'widget-management' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('widgets.index') }}">
                                <span class="sidebar-normal"> {{ __('Widget Management') }} </span>
                            </a>
                        </li>
                        @endcan
                        @can('manage-pages', App\Models\User::class)
                        <li class="nav-item{{ $activePage == 'page-management' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('pages.index') }}">
                                <span class="sidebar-normal"> {{ __('Page Management') }} </span>
                            </a>
                        </li>
                        @endcan
                        @can('manage-pages', App\Models\User::class)
                        <li class="nav-item{{ $activePage == 'orderfield-management' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('orderfields.index') }}">
                                <span class="sidebar-normal"> {{ __('Order Fields Management') }} </span>
                            </a>
                        </li>
                        @endcan

                    </ul>
                </div>
            </li>
            @endcan

            @can ('manage-users', App\Models\User::class)
            <li class="nav-item {{ ($menuParent ?? '' == 'laravel' || $activePage == 'dashboard') ? ' active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#systemManagement"
                    {{ ($menuParent ?? '' == 'laravel' || $activePage == 'dashboard') ? ' aria-expanded="true"' : '' }}>
                    <i class="material-icons">desktop_mac</i>
                    <p>{{ __('System Management') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ ($menuParent ?? '' == 'dashboard' || $menuParent ?? '' == 'laravel') ? ' show' : '' }}"
                    id="systemManagement">
                    <ul class="nav">
                        <li class="nav-item{{ $activePage == 'user-management' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('users.index') }}">
                                <span class="sidebar-normal"> {{ __('User Management') }} </span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'role-management' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('roles.index') }}">
                                <span class="sidebar-normal"> {{ __('Role Management') }} </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            @endcan
        </ul>
    </div>
</div>
