@extends('layouts.app', [
  'class' => 'off-canvas-sidebar',
  'classPage' => 'login-page',
  'activePage' => 'login',
  'title' => __('Register Your Account')
])

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-9 ml-auto mr-auto mb-3 text-center">
        <h3>{{ __('Welcome to Professional Research Help') }} </h3>

      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
        <form class="form" method="POST" action="{{ route('register') }}">
          @csrf

          <div class="card card-register card-hidden">
            <div class="card-header card-header-primary text-center">
              <h4 class="card-title">REGISTER YOUR ACCOUNT</h4>
            </div>
            <div class="card-body ">
              <div class="has-default{{ $errors->has('name') ? ' has-danger' : '' }} mb-3">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">face</i>
                      </span>
                    </div>
                    <input type="text" name="name" class="form-control" placeholder="{{ __('Name...') }}" value="{{ old('name') }}" required>
                    @if ($errors->has('name'))
                      <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                        <strong>{{ $errors->first('name') }}</strong>
                      </div>
                     @endif
                  </div>
                </div>
                <div class="has-default{{ $errors->has('email') ? ' has-danger' : '' }} mb-3">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">mail</i>
                      </span>
                    </div>
                    <input type="text" class="form-control" name="email" placeholder="{{ __('Email...') }}" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                      <div id="email-error" class="error text-danger pl-3" for="name" style="display: block;">
                        <strong>{{ $errors->first('email') }}</strong>
                      </div>
                     @endif
                  </div>
                </div>
                <div class="has-default{{ $errors->has('password') ? ' has-danger' : '' }} mb-3">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">lock_outline</i>
                      </span>
                    </div>
                    <input type="password" name="password" placeholder="{{ __('Password...') }}" class="form-control" required>
                    @if ($errors->has('password'))
                      <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                        <strong>{{ $errors->first('password') }}</strong>
                      </div>
                     @endif
                  </div>
                </div>
                <div class="has-default mb-3">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">lock_outline</i>
                      </span>
                    </div>
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="{{ __('Confirm Password...') }}" required>
                  </div>
                </div>
              <div class="form-check" style="padding: 10px;">
                  <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="policy" value="1" {{ old('policy', 1) ? 'checked' : '' }} >
                    <span class="form-check-sign">
                      <span class="check"></span>
                    </span>
                    {{ __('I agree with the ') }} <a href="#">{{ __('terms and conditions') }}</a>
                  </label>
                </div>
            </div>
            <div class="card-footer justify-content-center">
              <button type="submit" class="btn btn-primary btn-round mt-4">{{ __('Get Started') }}</button>
            </div>
          </div>
        </form>
        <div class="row">
          <div class="col-6"></div>
          <div class="col-6 text-right">
              Already Have an Account?<br>
              <a href="{{ route('login') }}" class="text-light">
                  {{ __('Login to Your account') }}
              </a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
<script>
  $(document).ready(function() {
    md.checkFullPageBackgroundImage();
    setTimeout(function() {
      // after 1000 ms we add the class animated to the login/register card
      $('.card').removeClass('card-hidden');
    }, 700);
  });
</script>
@endpush
