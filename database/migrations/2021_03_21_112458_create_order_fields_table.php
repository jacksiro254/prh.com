<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label')->nullable();
            $table->string('instruction')->nullable();
            $table->string('type')->nullable();
            $table->string('options')->nullable();
            $table->string('attributes')->nullable();
            $table->string('default_value')->nullable();
            $table->string('position')->default(0);
            $table->string('enabled')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_fields');
    }
}
